const passport = require('../middlewares/passport');
const models = require('../models');
const { goToProcess } = require('../service/process');
const { chromeDoesSnapShot, firefoxDoSnapShot } = require('../service/puppeteer');


const executeProject = async ctx => {
  await passport.authenticate('jwt', { session: false }, async(err, user) => {
    if (err) {
      ctx.throw(500, err.message);
    }
    if (user) {
      try {
        const projectId = ctx.params.number;
        let coolStart = await models.Workflow.count({
          where: { projectId }
        });
        let startTime = new Date();
        if (coolStart > 0) {
          await models.Workflow.update({
            startTime,
            status: 'In Process'
          }, {
            where: { projectId }
          }).catch(
            error => {
              ctx.throw(500, error);
            }
          );
        } else {
          await models.Workflow.create({
            startTime,
            projectId,
            status: 'In Process'
          }).catch(
            error => {
              ctx.throw(500, error);
            }
          );
        }
        // await chromeDoesSnapShot(
        //   'https://www.wunderground.com',
        //   `./snapShots/previous/5.5.5.png`,
        //   'viewport',
        //   10224,
        //   'Nexus 10')
        goToProcess(projectId)
        ctx.status = 200;

      } catch (err) {
        ctx.throw(500, err);
      }
    } else {
      ctx.throw(401);
    }
  })(ctx);
};

module.exports = {
  executeProject
};
