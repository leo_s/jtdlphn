const passport = require('../middlewares/passport');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');

const models = require('../models');

const uuid = require('uuid-v4');
const jwtSecret = {
  secret: 'jwt-secret'
};

async function fetchTokenPair(userId) {
  const newRefreshToken = uuid();
  await models.RefreshToken.create({
    token: newRefreshToken,
    userId
  });
  return {
    token: jwt.sign({ id: userId }, jwtSecret.secret),
    refreshToken: newRefreshToken
  };
}

function fetchExpireTokenPair(userId) {
  return {
    token: jwt.sign({ id: userId }, jwtSecret.secret, { expiresIn: '1ms' }),
    refreshToken: '',
    success: true
  };
}

const signUp = async ctx => {
  try {
    let { name, email, password } = ctx.request.body;
    const encryptedPassword = await bcrypt.hash(password, 12);
    const dbRes = await models.User.create({
      name,
      email,
      password: encryptedPassword
    }).catch(
      error => {
        if (error.name.localeCompare('SequelizeValidationError') === 0) {
          ctx.throw(400, error.message);
        } else if (error.name.localeCompare('SequelizeUniqueConstraintError') === 0) {
          ctx.throw(400, `${email} had already registered, try another please!`);
        } else {
          throw error;
        }
      }
    );
    ctx.status = 200;
    ctx.body = await fetchTokenPair(dbRes.dataValues.id);
  } catch (err) {
    ctx.throw(500, err);
  }
};

const login = async ctx => {
  await passport.authenticate('login', {}, async(err, user) => {
    if (!user) {
      ctx.throw(403, 'Incorrect login/password');
    }
    if (err) {
      ctx.throw(500, err.message);
    }
    ctx.status = 200;
    ctx.body = await fetchTokenPair(user.id);
  })(ctx);
};

const refreshToken = async ctx => {
  try {
    let { refreshToken } = ctx.request.body;
    const dbToken = await models.RefreshToken.findOne({
      where: {
        token: refreshToken
      }
    });
    if (!dbToken) {
      ctx.throw(403);
    }
    await models.RefreshToken.destroy({
      where: {
        id: dbToken.id
      }
    });
    ctx.body = await fetchTokenPair(dbToken.userId);
  } catch (err) {
    ctx.throw(500, err);
  }
};

const logout = async ctx => {
  await passport.authenticate('jwt', { session: false }, async(err, user) => {
    if (!user) {
      ctx.throw(401);
    }
    if (err) {
      ctx.throw(500, err.message);
    }
    await models.RefreshToken.destroy({
      where: {
        userId: user.id
      }
    });
    ctx.body = fetchExpireTokenPair(user.id);
  })(ctx);
};

module.exports = {
  signUp,
  login,
  refreshToken,
  logout,
  fetchTokenPair
};
