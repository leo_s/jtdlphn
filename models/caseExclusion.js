const Sequelize = require('sequelize');

module.exports = sequelize => {
  return sequelize.define('CaseExclusion', {
    exclusion: {
      type: Sequelize.STRING(100),
      allowNull: false
    }
  }, {
    tableName: 'caseExclusion',
    charset: 'utf8',
    collate: 'utf8_unicode_ci'
  });
};
