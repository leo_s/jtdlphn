const Sequelize = require('sequelize');

module.exports = sequelize => {
  return sequelize.define('RunResult', {
    firstUse: {
      type: Sequelize.BOOLEAN,
      allowNull: false,
      defaultValue: true
    },
    mismatch: {
      type: Sequelize.INTEGER,
      allowNull: true,
      defaultValue: null
    }
  }, {
    tableName: 'runResult',
    charset: 'utf8',
    collate: 'utf8_unicode_ci'
  });
};
