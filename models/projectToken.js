const Sequelize = require('sequelize');

module.exports = sequelize => {
  return sequelize.define('ProjectToken', {
    token: {
      type: Sequelize.STRING(50),
      allowNull: false
    }
  }, {
    tableName: 'projectToken',
    charset: 'utf8',
    collate: 'utf8_unicode_ci'
  });
};
