const models = require('../models');
const fs = require('fs');
const { puppeteerDoesSnapShot } = require('./puppeteer');
const {
  setItemFirstUseTrue,
  setItemFirstUseFalse,
  setClientFirstUseFalse,
  setCaseFirstUseFalse
} = require('./runResultDBOperation');

const goToProcess = async projectId => {
  let clients = await models.Client.findAll({
    where: { projectId },
    attributes: [['id', 'clientId'], 'browser', 'type', 'width', 'height', 'device', ['firstUse', 'clientFirstUse']]
  });
  let cases = await models.Case.findAll({
    where: { projectId },
    attributes: [['id', 'caseId'], 'url', ['firstUse', 'caseFirstUse']]
  });
  const buildPath = ['current', 'previous', 'difference'];
  buildPath.forEach(pathItem => {
    if (!fs.existsSync(`./snapShots/${pathItem}/${projectId}`)) {
      fs.mkdirSync(`./snapShots/${pathItem}/${projectId}`);
    }
  })
  // if (!fs.existsSync(`./snapShots/current/${projectId}`)) {
  //   fs.mkdirSync(`./snapShots/current/${projectId}`);
  // }
  // if (!fs.existsSync(`./snapShots/previous/${projectId}`)) {
  //   fs.mkdirSync(`./snapShots/previous/${projectId}`);
  // }
  const promises = [];
  clients.map(client => {
    cases.map(projectCase => {
      const { caseId, url, caseFirstUse } = projectCase.dataValues;
      const { clientId, browser, type, width, height, device, clientFirstUse } = client.dataValues;
      let path;
      let item;
      if (caseFirstUse || clientFirstUse) {
        setItemFirstUseTrue(projectId, caseId, clientId);
        path = `./snapShots/previous/${projectId}/${caseId}.${clientId}.png`;
        item = { firstUse: true };
      } else {
        path = `./snapShots/current/${projectId}/${caseId}.${clientId}.png`;
        setItemFirstUseFalse(projectId, caseId, clientId);
        item = { firstUse: false, projectId, caseId, clientId };
      }
      let puppeteerUrl;
      if (url.includes('https://www.') || (url.includes('http://www.'))) {
        puppeteerUrl = url;
      } else if (url.includes('www.')) {
        puppeteerUrl = `https://${url}`;
      } else {
        puppeteerUrl = `https://www.${url}`;
      }
      promises.push(puppeteerDoesSnapShot(puppeteerUrl, browser, path, type, width, height, device, item));
      // if (browser.localeCompare('chrome') === 0) {
      //   promises.push(chromeDoesSnapShot(puppeteerUrl, browser, path, type, width, height, device, item));
      // } else {
      //   promises.push(firefoxDoSnapShot(puppeteerUrl, browser, path, type, width, height, device, item));
      // }
    });
  });
  await Promise.all(promises).then(
    async response => {
      let startTime = await models.Workflow.find({
        where: { projectId },
        attributes: ['startTime']
      });
      let duration = new Date() - startTime.dataValues.startTime;
      models.Workflow.update({
        duration,
        status: 'Pass'
      }, {
        where: { projectId }
      });
    }
  );

  clients.map(client => {
    setClientFirstUseFalse(client.dataValues.clientId);
  });
  cases.map(projectCase => {
    setCaseFirstUseFalse(projectCase.dataValues.caseId);
  });
};

module.exports = {
  goToProcess
};
