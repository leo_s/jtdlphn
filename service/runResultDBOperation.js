const models = require('../models');

const setItemFirstUseTrue = async(ProjectId, CaseId, ClientId) => {
  await models.RunResult.create({
    ProjectId,
    CaseId,
    ClientId
  });
};
const setItemFirstUseFalse = async(ProjectId, CaseId, ClientId) => {
  await models.RunResult.update({
    firstUse: false
  }, {
    where: { ProjectId, CaseId, ClientId }
  });
};

const setClientFirstUseFalse = id => {
  models.Client.update({
    firstUse: false
  }, {
    where: { id }
  });
};

const setCaseFirstUseFalse = id => {
  models.Case.update({
    firstUse: false
  }, {
    where: { id }
  });
};

const setMismatch = async(mismatch, ProjectId, CaseId, ClientId) => {
  await models.RunResult.update({
    mismatch
  }, {
    where: { ProjectId, CaseId, ClientId }
  });
};

module.exports = {
  setItemFirstUseTrue,
  setItemFirstUseFalse,
  setClientFirstUseFalse,
  setCaseFirstUseFalse,
  setMismatch
};
