const puppeteer = require('puppeteer');
const puppeteerFirefox = require('puppeteer-firefox');
const devices = require('puppeteer/DeviceDescriptors');
// const devices = require('puppeteer-firefox/DeviceDescriptors');
const GalaxyS5 = devices['Galaxy S5'];
const Pixel2 = devices['Pixel 2'];
const Pixel2XL = devices['Pixel 2 XL'];
const iPhone5 = devices['iPhone 5'];
const iPhoneSE = devices['iPhone SE'];
const iPhone6 = devices['iPhone 6'];
const iPhone7 = devices['iPhone 7'];
const iPhone6Plus = devices['iPhone 6 Plus'];
const iPhoneX = devices['iPhone X'];
const iPad = devices['iPad'];
const iPadPro = devices['iPad Pro'];
const Nexus10 = devices['Nexus 10'];
const {
  snapCompare
} = require('./comparison');

const clientDevices = {
  'Galaxy S5': GalaxyS5,
  'Pixel 2': Pixel2,
  'Pixel 2 XL': Pixel2XL,
  'iPhone 5': iPhone5,
  'iPhone SE': iPhoneSE,
  'iPhone 6': iPhone6,
  'iPhone 7': iPhone7,
  'iPhone 6 Plus': iPhone6Plus,
  'iPhone X': iPhoneX,
  'iPad': iPad,
  'iPad Pro': iPadPro,
  'Nexus 10': Nexus10
};

// const chromeDoesSnapShot = async(url, path, type, width, height, device, item) => {
const puppeteerDoesSnapShot = async(url, clientBrowser, path, type, width, height, device, item) => {
  let browser;
  if (clientBrowser.localeCompare('chrome') === 0) {
    // promises.push(chromeDoesSnapShot(puppeteerUrl, browser, path, type, width, height, device, item));
    browser = await puppeteer.launch();
  } else {
    // promises.push(firefoxDoSnapShot(puppeteerUrl, browser, path, type, width, height, device, item));
    browser = await puppeteerFirefox.launch();
  }
  const page = await browser.newPage();
  if (type.localeCompare('viewport') === 0) {
    await page.setViewport({ width, height });
  } else {
    await page.emulate(clientDevices[device]);
  }
  // await page.waitFor(60000);
  await page.goto(url);
  await page.screenshot({
    fullPage: true,
    path: path
  });
  await browser.close();
  if (!item.firstUse) {
    snapCompare(item);
  }
};

// const firefoxDoSnapShot = async(url, path, type, width, device) => {
// };

module.exports = {
  puppeteerDoesSnapShot
  // firefoxDoSnapShot
};
