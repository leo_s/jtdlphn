const request = require('supertest');
const app = require('../index.js');
const models = require('../models');
const server = app.listen(8891);

/* global beforeAll afterAll expect test sandbox */
/* globals sandbox:true */
beforeAll(async() => {
  await models.sequelize.sync({ force: true });
  sandbox = await request(server)
    .post(`/api/v1/signUp`)
    .send({
      password: '1qaz!QAZ',
      name: 'User3',
      email: 'user3@gmail.com'
    });
});

afterAll(() => {
  server.close();
  console.log('server closed!');
});

test('Add two project', async() => {
  const userDataResponse = await request(server)
    .post(`/api/v1/project`)
    .set('Authorization', `Bearer ${sandbox.body.token}`)
    .send({
      name: 'project N1',
      description: 'Best project for fast exploration ...'
    });
  expect(userDataResponse.status).toEqual(200);
  await request(server)
    .post(`/api/v1/project`)
    .set('Authorization', `Bearer ${sandbox.body.token}`)
    .send({
      name: 'project N2',
      description: 'Good project for thorough inspection ...'
      // projectClients: JSON.stringify(['1', '2'])
    });
  expect(userDataResponse.status).toEqual(200);
});

test('Add two Clients for project N4', async() => {
  // expect(signUpResponse.status).toEqual(200);
  const firstResponse = await request(server)
    .post(`/api/v1/client`)
    .set('Authorization', `Bearer ${sandbox.body.token}`)
    .send({
      name: 'client N1',
      browser: 'chrome',
      type: 'viewport',
      width: 450,
      height: 350,
      ProjectId: 2
    });
  expect(firstResponse.status).toEqual(200);
  const secondResponse = await request(server)
    .post(`/api/v1/client`)
    .set('Authorization', `Bearer ${sandbox.body.token}`)
    .send({
      name: 'client N2',
      browser: 'chrome',
      type: 'emulation',
      device: 'Galaxy S5',
      ProjectId: 2
    });
  expect(secondResponse.status).toEqual(200);
});
test('Get Client list for project N4', async() => {
  // expect(signUpResponse.status).toEqual(200);
  const Response = await request(server)
    .get(`/api/v1/clients/2`)
    .set('Authorization', `Bearer ${sandbox.body.token}`)
    .send();
  expect(Response.status).toEqual(200);
  expect(Response.body).toMatchObject([
    {
      'name': 'client N1',
      'browser': 'chrome',
      'type': 'viewport',
      'width': 450,
      'height': 350,
      'device': null
    }, {
      'name': 'client N2',
      'browser': 'chrome',
      'type': 'emulation',
      'width': null,
      'height': null,
      'device': 'Galaxy S5'
    }
  ]
  );
});

test('Update Client N2', async() => {
  const Response = await request(server)
    .put(`/api/v1/client`)
    .set('Authorization', `Bearer ${sandbox.body.token}`)
    .send({
      id: 2,
      name: 'client N2',
      browser: 'firefox',
      type: 'viewport',
      width: 720,
      height: 520,
      device: null
    });
  expect(Response.status).toEqual(200);
});

test('Get Client N2', async() => {
  // expect(signUpResponse.status).toEqual(200);
  const Response = await request(server)
    .get(`/api/v1/client/2`)
    .set('Authorization', `Bearer ${sandbox.body.token}`)
    .send();
  expect(Response.status).toEqual(200);
  expect(Response.body).toMatchObject(
    {
      'name': 'client N2',
      'browser': 'firefox',
      'type': 'viewport',
      'width': 720,
      'height': 520,
      'device': null
    }
  );
});

test('Delete Client N2', async() => {
  const Response = await request(server)
    .delete(`/api/v1/client/2`)
    .set('Authorization', `Bearer ${sandbox.body.token}`)
    .send();
  expect(Response.status).toEqual(200);
});

test('Get Client list for project N4', async() => {
  // expect(signUpResponse.status).toEqual(200);
  const Response = await request(server)
    .get(`/api/v1/clients/2`)
    .set('Authorization', `Bearer ${sandbox.body.token}`)
    .send();
  expect(Response.status).toEqual(200);
  expect(Response.body).toMatchObject([
    {
      'name': 'client N1',
      'browser': 'chrome',
      'type': 'viewport',
      'width': 450,
      'height': 350,
      'device': null
    }
  ]
  );
});
