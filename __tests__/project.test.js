const request = require('supertest');
const app = require('../index.js');
const models = require('../models');
const server = app.listen(8893);

/* global beforeAll afterAll expect test sandbox */
/* globals sandbox:true */
beforeAll(async() => {
  await models.sequelize.query('SET FOREIGN_KEY_CHECKS = 0');
  await models.sequelize.sync({ force: true });
  sandbox = await request(server)
    .post(`/api/v1/signUp`)
    .send({
      password: '1qaz!QAZ',
      name: 'User5',
      email: 'user5@gmail.com'
    });
});

afterAll(() => {
  server.close();
  console.log('server closed!');
});

test('Add two project', async() => {
  const userDataResponse = await request(server)
    .post(`/api/v1/project`)
    .set('Authorization', `Bearer ${sandbox.body.token}`)
    .send({
      name: 'project N5',
      description: 'Best project for fast exploration ...'
      // projectCases: JSON.stringify(['1']),
      // projectClients: JSON.stringify(['1', '2'])
    });
  expect(userDataResponse.status).toEqual(200);
  await request(server)
    .post(`/api/v1/project`)
    .set('Authorization', `Bearer ${sandbox.body.token}`)
    .send({
      name: 'project N6',
      description: 'Good project for thorough inspection ...'
      // projectClients: JSON.stringify(['1', '2'])
    });
  expect(userDataResponse.status).toEqual(200);
});

test('Get Project list', async() => {
  // expect(signUpResponse.status).toEqual(200);
  const Response = await request(server)
    .get(`/api/v1/projects`)
    .set('Authorization', `Bearer ${sandbox.body.token}`)
    .send();
  expect(Response.status).toEqual(200);
  expect(Response.body).toMatchObject([
    {
      'id': 1,
      'name': 'project N5'
    }, {
      'id': 2,
      'name': 'project N6'
    }
  ]
  );
});
test('Update Project N5', async() => {
  // expect(signUpResponse.status).toEqual(200);
  const Response = await request(server)
    .put(`/api/v1/project`)
    .set('Authorization', `Bearer ${sandbox.body.token}`)
    .send({
      id: 1,
      name: 'Modified project N5',
      description: 'Modified Best project for fast exploration ...'
      // projectClients: JSON.stringify(['1', '2'])
    });
  expect(Response.status).toEqual(200);
});

test('Get Project N5', async() => {
  // expect(signUpResponse.status).toEqual(200);
  const Response = await request(server)
    .get(`/api/v1/project/1`)
    .set('Authorization', `Bearer ${sandbox.body.token}`)
    .send();
  expect(Response.status).toEqual(200);
  expect(Response.body).toMatchObject(
    {
      'id': 1,
      'name': 'Modified project N5',
      'description': 'Modified Best project for fast exploration ...'
    }
  );
});

test('Delete Project N5', async() => {
  const Response = await request(server)
    .delete(`/api/v1/project/1`)
    .set('Authorization', `Bearer ${sandbox.body.token}`)
    .send();
  expect(Response.status).toEqual(200);
});
test('Get Project list after removal', async() => {
  // expect(signUpResponse.status).toEqual(200);
  const Response = await request(server)
    .get(`/api/v1/projects`)
    .set('Authorization', `Bearer ${sandbox.body.token}`)
    .send();
  expect(Response.status).toEqual(200);
  expect(Response.body).toMatchObject([
    {
      'id': 2,
      'name': 'project N6'
    }
  ]
  );
});
