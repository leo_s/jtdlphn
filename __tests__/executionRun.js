const request = require('supertest');
const app = require('../index.js');
const server = app.listen(8889);

/* global beforeAll afterAll expect test sandbox */
/* globals sandbox:true */
beforeAll(async() => {
  sandbox = await request(server)
    .post('/api/v1/login')
    .send({
      email: 'user1@gmail.com',
      password: '1qaz!QAZ'
    });

});

afterAll(() => {
  server.close();
  console.log('server closed!');
});

test('Get item list for project N2', async() => {
  // expect(signUpResponse.status).toEqual(200);
  const Response = await request(server)
    .get(`/api/v1/execute/2`)
    .set('Authorization', `Bearer ${sandbox.body.token}`)
    .send();
  expect(Response.status).toEqual(200);
  expect(Response.body).toMatchObject([
    {
      'id': 1,
      'name': 'case N1',
      'description': 'First case for ...',
      'url': 'google.com'
    }, {
      'id': 2,
      'name': 'case N2',
      'description': 'Second case for ...',
      'url': 'yahoo.com'
    }
  ]
  );
});

