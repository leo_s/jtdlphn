const request = require('supertest');
const app = require('../index.js');
const models = require('../models');
const server = app.listen(8889);

/* global describe beforeAll afterAll expect test sandbox */
/* globals sandbox:true */
beforeAll(async() => {
  await models.sequelize.sync({ force: true });
  sandbox = await request(server)
    .post(`/api/v1/signUp`)
    .send({
      password: '1qaz!QAZ',
      name: 'User1',
      email: 'user1@gmail.com'
    });
});

afterAll(() => {
  server.close();
  console.log('server closed!');
});

describe('entity test', async() => {
  test('Add Case', async() => {
    const firstResponse = await request(server)
      .post(`/api/v1/addCase`)
      .set('Authorization', `Bearer ${sandbox.body.token}`)
      .send({
        name: 'case N1',
        description: 'First case for ...',
        url: 'google.com',
        components: '',
        exclusions: ''
      });
    expect(firstResponse.status).toEqual(200);
    const secondResponse = await request(server)
      .post(`/api/v1/addCase`)
      .set('Authorization', `Bearer ${sandbox.body.token}`)
      .send({
        name: 'case N2',
        description: 'Second case for ...',
        url: 'google.ru',
        components: '',
        exclusions: ''
      });
    expect(secondResponse.status).toEqual(200);
  });
  test(' Get the Case list', async() => {
    const userDataResponse = await request(server)
      .get(`/api/v1/getCaseList`)
      .set('Authorization', `Bearer ${sandbox.body.token}`)
      .send();
    expect(userDataResponse.body).toEqual([
      {
        'id': 1,
        'name': 'case N1',
        'description': 'First case for ...'
      }, {
        'id': 2,
        'name': 'case N2',
        'description': 'Second case for ...'
      }
    ]
    );
    expect(userDataResponse.status).toEqual(200);
  });
  test(' Update second Case', async() => {
    const userDataResponse = await request(server)
      .put(`/api/v1/varyCase`)
      .set('Authorization', `Bearer ${sandbox.body.token}`)
      .send({
        id: 2,
        name: 'case N2',
        description: 'Second case for ...',
        url: 'rbc.ru',
        components: '',
        exclusions: ''
      });
    expect(userDataResponse.status).toEqual(200);
  });
  test(' Get second Case', async() => {
    const userDataResponse = await request(server)
      .get(`/api/v1/getCase/2`)
      .set('Authorization', `Bearer ${sandbox.body.token}`)
      // .set('Authorization', `Bearer sdfgsdfgsdf`)
      .send();
    expect(userDataResponse.body).toMatchObject(
      {
        'id': 2,
        'name': 'case N2',
        'description': 'Second case for ...',
        'components': '',
        'exclusions': '',
        'url': 'rbc.ru'
      }
    );
  });
  test('Create two Clients', async() => {
    // expect(signUpResponse.status).toEqual(200);
    const firstResponse = await request(server)
      .post(`/api/v1/addClient`)
      .set('Authorization', `Bearer ${sandbox.body.token}`)
      .send({
        name: 'client N1',
        browser: 'chrome',
        type: 'viewport',
        width: 320,
        device: 'none'
      });
    expect(firstResponse.status).toEqual(200);
    const secondResponse = await request(server)
      .post(`/api/v1/addClient`)
      .set('Authorization', `Bearer ${sandbox.body.token}`)
      .send({
        name: 'client N2',
        browser: 'chrome',
        type: 'viewport',
        width: 760,
        device: 'none'
      });
    expect(secondResponse.status).toEqual(200);
  });
  test('Get the Client list', async() => {
    // expect(signUpResponse.status).toEqual(200);
    const userDataResponse = await request(server)
      .get(`/api/v1/getClientList`)
      .set('Authorization', `Bearer ${sandbox.body.token}`)
      .send();
    expect(userDataResponse.body).toEqual([
      {
        'id': 1,
        'name': 'client N1',
        'browser': 'chrome',
        'type': 'viewport',
        'width': 320,
        'device': 'none'
      }, {
        'id': 2,
        'name': 'client N2',
        'browser': 'chrome',
        'type': 'viewport',
        'width': 760,
        'device': 'none'
      }
    ]
    );
    expect(userDataResponse.status).toEqual(200);
  });
  test(' Update second Client', async() => {
    const userDataResponse = await request(server)
      .put(`/api/v1/varyClient`)
      .set('Authorization', `Bearer ${sandbox.body.token}`)
      .send({
        id: 2,
        name: 'client N2',
        browser: 'chrome',
        type: 'viewport',
        width: 555,
        device: 'none'
      });
    expect(userDataResponse.status).toEqual(200);
  });

  test(' Get second Client', async() => {
    const userDataResponse = await request(server)
      .get(`/api/v1/getClient/2`)
      .set('Authorization', `Bearer ${sandbox.body.token}`)
      .send();
    expect(userDataResponse.body).toMatchObject(
      {
        'id': 2,
        'name': 'client N2',
        'browser': 'chrome',
        'type': 'viewport',
        'width': 555,
        'device': 'none'
      }
    );
  });

  test('Add two project', async() => {
    const userDataResponse = await request(server)
      .post(`/api/v1/addProject`)
      .set('Authorization', `Bearer ${sandbox.body.token}`)
      .send({
        name: 'project N1',
        projectCases: JSON.stringify(['1']),
        projectClients: JSON.stringify(['1', '2'])
      });
    expect(userDataResponse.status).toEqual(200);
    await request(server)
      .post(`/api/v1/addProject`)
      .set('Authorization', `Bearer ${sandbox.body.token}`)
      .send({
        name: 'project N2',
        projectCases: JSON.stringify(['2']),
        projectClients: JSON.stringify(['1', '2'])
      });
    expect(userDataResponse.status).toEqual(200);
  });
  test('Get the Project list', async() => {
    const userDataResponse = await request(server)
      .get(`/api/v1/getProjectList`)
      .set('Authorization', `Bearer ${sandbox.body.token}`)
      .send();
    expect(userDataResponse.status).toEqual(200);
    expect(userDataResponse.body).toEqual([
      {
        'id': 1,
        'name': 'project N1'
      }, {
        'id': 2,
        'name': 'project N2'
      }
    ]
    );
  });
  test('Update second Project', async() => {
    const userDataResponse = await request(server)
      .put(`/api/v1/varyProject`)
      .set('Authorization', `Bearer ${sandbox.body.token}`)
      .send({
        id: 2,
        name: 'project N2 modify',
        projectCases: JSON.stringify(['1']),
        projectClients: JSON.stringify(['1', '2'])
      });
    expect(userDataResponse.status).toEqual(200);
  });
  test('Get second Project', async() => {
    const userDataResponse = await request(server)
      .get(`/api/v1/getProject/2`)
      .set('Authorization', `Bearer ${sandbox.body.token}`)
      .send();
    expect(userDataResponse.body).toMatchObject([
      {
        'id': 2,
        'case_id': 1,
        'client_id': 1
      },
      {
        'id': 2,
        'case_id': 1,
        'client_id': 2
      }
    ]
    );
  });

  test(' Test workflow', async() => {
    // expect(signUpResponse.status).toEqual(200);
    const logResponse = await request(server)
      .post(`/api/v1/logWorkflow`)
      .set('Authorization', `Bearer ${sandbox.body.token}`)
      .send({
        projectId: '1'
      });
    expect(logResponse.status).toEqual(200);
    const udateResponse = await request(server)
      .post(`/api/v1/udateWorkflow`)
      .set('Authorization', `Bearer ${sandbox.body.token}`)
      .send({
        projectId: '1',
        status: 'Pass'
      });
    expect(udateResponse.status).toEqual(200);
  });
});
